/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Services;

import Com.Wui.Framework.IdeJRE.Servers.JxbrowserBridgeServer;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.JSValue;
import com.teamdev.jxbrowser.chromium.events.ScriptContextAdapter;
import com.teamdev.jxbrowser.chromium.events.ScriptContextEvent;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.JComponent;

public final class JxbrowserService {

    private static final Logger logger = LogManager.getLogger(JxbrowserService.class);
    private BrowserView browserView;

    public JxbrowserService(final String $projectId) {
        browserView = new BrowserView();
        browserView.getBrowser().addScriptContextListener(new ScriptContextAdapter() {
            @Override
            public void onScriptContextCreated(final ScriptContextEvent event) { // checkstyle:disable-line
                Browser browser = event.getBrowser();
                JSValue window = browser.executeJavaScriptAndReturnValue("window");
                window.asObject().setProperty("java", new JxbrowserBridgeServer($projectId, browser));
            }
        });
    }

    public JComponent getViewComponent() {
        return browserView;
    }

    public String getUrl() {
        return browserView.getBrowser().getURL();
    }

    public void setUrl(final String $url) {
        browserView.getBrowser().loadURL($url);
    }
}
