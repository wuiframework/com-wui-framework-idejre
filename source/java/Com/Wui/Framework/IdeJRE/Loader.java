/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE;

import Com.Wui.Framework.IdeJRE.Interfaces.IItem;
import Com.Wui.Framework.IdeJRE.Interfaces.IMenu;
import Com.Wui.Framework.IdeJRE.Interfaces.IPartFactory;
import Com.Wui.Framework.IdeJRE.Interfaces.IPluginRuntime;
import Com.Wui.Framework.IdeJRE.Interfaces.IView;
import Com.Wui.Framework.IdeJRE.Services.JxbrowserService;

import java.io.IOException;

public class Loader { // checkstyle:disable-line

    private static IPluginRuntime pluginRuntime;

    public static void Load(final IPluginRuntime $pluginRuntime, final String $projectId) throws IOException {
        System.out.println("LOADING");
        if (pluginRuntime == null) {
            pluginRuntime = $pluginRuntime;
        }

        IPartFactory factory = pluginRuntime.getPartFactory($projectId);

        IMenu dynamicMenu = factory.getDefinedMenu("DynamicMenu");
        IItem menuItem = factory.createMenuItem("menuItemId");
        IMenu submenu = factory.createMenu("subMenuId");
        submenu.setLabel("Submenu");
        submenu.addItem(menuItem);
        menuItem.setLabel("Item");
        menuItem.setIconPath("resource/icon.png");
        dynamicMenu.addItem(submenu);

        IView view = factory.getDefinedView("WuiView1");
        JxbrowserService browserService = new JxbrowserService($projectId);
        browserService.setUrl("www.nxp.com");
        view.setContent(browserService.getViewComponent());

        /*
        String tempDir = System.getProperty("java.io.tmpdir");
        String extractPath = tempDir + $pluginRuntime.getPluginId() + "/target";
        String zipPath = extractPath + ".zip";
        String targetUrl = "http://www.nxp.com";
        String resourcePath = null;
        if (FileSystemConnector.ExistsResource("target.zip")) {
            resourcePath = "target.zip";
        } else if (FileSystemConnector.ExistsResource("resource/target.zip")) {
            resourcePath = "resource/target.zip";
        }

        String windowTitle = "Wui browser test";
        String windowIcon = "resource/graphics/icon13x13.png";

        String appConfigPath = "resource/configs/default.config.jsonp";
        if (FileSystemConnector.ExistsResource(appConfigPath)) {
            String appData = FileSystemConnector.ReadResource(appConfigPath);
            appData = appData.substring(appData.indexOf("({") + 1, appData.lastIndexOf("});") + 1);
            IEnvironmentConfiguration dataObj = ObjectDecoder.JSON(appData, IEnvironmentConfiguration.class);
            windowTitle = dataObj.appName;
        }

       if (resourcePath != null) {
            FileSystemConnector.Delete(extractPath);
            FileSystemConnector.ExtractJarResource(resourcePath, zipPath);
            FileSystemConnector.Unpack(zipPath, new ArchiveOptions().Output(extractPath));

            targetUrl = "file://" + tempDir + "/" + $pluginRuntime.getPluginId() + "/target/target/index.html";

            String targetConfigPath = tempDir + "/" + $pluginRuntime.getPluginId() + "/target/target/wuirunner.config.jsonp";
            if (FileSystemConnector.ExistsResource(targetConfigPath)) {
                String targetData = FileSystemConnector.ReadResource(targetConfigPath);
                targetData = targetData.substring(targetData.indexOf("({") + 1, targetData.lastIndexOf("});") + 1);
                IAppConfiguration dataObj = ObjectDecoder.JSON(targetData, IAppConfiguration.class);
                String startPage = dataObj.startPage;
                windowTitle = dataObj.window.title;
                windowIcon = tempDir + "/" + $pluginRuntime.getPluginId() + "/" + dataObj.window.icon;
                if (!startPage.isEmpty() && !startPage.contains("<?")) {
                    targetUrl += "#" + startPage;
                }
            }
        }
        */
    }

    public static IPluginRuntime getPluginRuntime() {
        return pluginRuntime;
    }
}
