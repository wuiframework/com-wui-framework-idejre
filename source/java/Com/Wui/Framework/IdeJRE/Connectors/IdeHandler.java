/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Connectors;

import Com.Wui.Framework.IdeJRE.Loader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class IdeHandler { // checkstyle:disable-line

    private static final Logger logger = LogManager.getLogger(IdeHandler.class);

    public static List<String> OpenFileChooser(final String $path, final ArrayList<String> $extensions, final int $clientId) {
        String[] extensions = $extensions == null ? new String[]{} : $extensions.toArray(new String[0]);
        return Loader.getPluginRuntime().openFileChooser($path, extensions, $clientId);
    }

    public static boolean OpenInfoDialog(final String $title, final String $message, final int $clientId) {
        return Loader.getPluginRuntime().openInfoDialog($title, $message, $clientId);
    }

    public static String getWorkspaceRoot(final int $clientId) {
        return Loader.getPluginRuntime().getWorkspaceRoot($clientId);
    }
}
