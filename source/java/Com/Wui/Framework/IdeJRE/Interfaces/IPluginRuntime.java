/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Interfaces;

import java.util.List;

public interface IPluginRuntime {
    void linkClientWithProject(int $clientId, String $projectId);

    String getProjectId(int $clientId);

    String getWorkspaceRoot(int $clientId);

    String getPluginId();

    boolean openInfoDialog(String $title, String $message, int $clientId);

    List<String> openFileChooser(String $path, String[] $extensions, int $clientId);

    IPartFactory getPartFactory(String $projectId);
}
