/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Interfaces;

public interface IPartFactory {

    IMenu getDefinedMenu(String $menuId);

    IMenu createMenu(String $menuId);

    IItem createMenuItem(String $menuItemId);

    ITool getDefinedTool(String $toolId);

    IView getDefinedView(String $viewId);
}
