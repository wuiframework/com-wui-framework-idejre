/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Interfaces;

public class IEnvironmentConfiguration {
    /* checkstyle:disable: JavadocVariable|VisibilityModifier|MemberName */
    public String $interface;
    public String projectName;
    public String projectVersion;
    public String appName;
    public String projectDescription;
    public String projectAuthor;
    public String projectLicense;
    public String buildTime;
    public IIdePartConfiguration[] views;
    public IIdePartConfiguration[] menus;
    public IIdePartConfiguration[] tools;
    /* checkstyle:enable */
}
