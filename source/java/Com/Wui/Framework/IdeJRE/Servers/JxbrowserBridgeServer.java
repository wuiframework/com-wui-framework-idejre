/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Servers;

import Com.Wui.Framework.IdeJRE.Loader;
import Com.Wui.Framework.Jcommons.Resolvers.Protocols.WebServiceProtocol;
import Com.Wui.Framework.Jcommons.Servers.BaseServer;
import Com.Wui.Framework.Jcommons.Utils.ObjectEncoder;
import com.teamdev.jxbrowser.chromium.Browser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JxbrowserBridgeServer extends BaseServer {
    private static Logger logger = LogManager.getLogger(JxbrowserBridgeServer.class);
    private final Browser browser;
    private final String projectId;

    public JxbrowserBridgeServer(final String $projectId, final Browser $browser) {
        super();
        projectId = $projectId;
        browser = $browser;
    }

    @Override
    public void send(final Integer $clientId, final String $data) {
        try {
            Loader.getPluginRuntime().linkClientWithProject($clientId, projectId);
            super.send($clientId, $data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void resolve(final WebServiceProtocol $data) {
        $data.origin = "http://127.0.0.1/";
        browser.executeJavaScriptAndReturnValue(
                "Com.Wui.Framework.Commons.WebServiceApi.Clients.JxbrowserBridgeClient.Callback('" + ObjectEncoder.JSON($data) + "')");
    }
}
