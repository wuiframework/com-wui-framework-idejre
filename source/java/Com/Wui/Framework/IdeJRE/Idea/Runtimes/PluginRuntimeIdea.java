/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Idea.Runtimes;

import Com.Wui.Framework.IdeJRE.Idea.Factories.IdeaPartFactory;
import Com.Wui.Framework.IdeJRE.Interfaces.IPartFactory;
import Com.Wui.Framework.IdeJRE.Interfaces.IPluginRuntime;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.fileChooser.FileChooserDialog;
import com.intellij.openapi.fileChooser.FileChooserFactory;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class PluginRuntimeIdea implements IPluginRuntime {

    private static final Logger logger = LogManager.getLogger(PluginRuntimeIdea.class);

    private final String pluginId;
    private final Map<Integer, String> projectIdPerClientId; // todo : remove on project close?

    public PluginRuntimeIdea(final String $pluginId) {
        pluginId = $pluginId;
        projectIdPerClientId = new ConcurrentHashMap<>();
    }

    @Override
    public void linkClientWithProject(final int $clientId, final String $projectId) {
        if ($projectId == null) {
            return;
        }
        projectIdPerClientId.putIfAbsent($clientId, $projectId);
    }

    @Override
    public String getProjectId(final int $clientId) {
        return projectIdPerClientId.get($clientId);
    }

    @Override
    public String getWorkspaceRoot(final int $clientId) {
        Project project = getProject(getProjectId($clientId));
        if (project == null) {
            return "";
        }
        return project.getBasePath();
    }

    @Override
    public String getPluginId() {
        return pluginId;
    }


    @Override
    public boolean openInfoDialog(final String $title, final String $message, final int $clientId) {
        AtomicBoolean returnValue = new AtomicBoolean(false);
        Runnable openInfoDialog = () -> returnValue
                .set(Messages.showDialog(getProject(projectIdPerClientId.get($clientId)), $message, $title,
                        new String[]{Messages.OK_BUTTON}, 0, Messages.getInformationIcon()) == 0);
        ApplicationManager.getApplication().invokeAndWait(openInfoDialog);
        return returnValue.get();
    }

    @Override
    public List<String> openFileChooser(final String $path, final String[] $extensions, final int $clientId) {
        List<VirtualFile> chosenFiles = new ArrayList<>();
        Project project = getProject(projectIdPerClientId.get($clientId));
        Runnable openFileChooser = () -> {
            FileChooserDescriptor descriptor =
                    new FileChooserDescriptor(true, false, true, true, false, true);
            FileChooserDialog chooser =
                    FileChooserFactory.getInstance().createFileChooser(descriptor
                            .withFileFilter(virtualFile -> {
                                if (!virtualFile.isDirectory()) {
                                    for (String extension : $extensions) {
                                        if (virtualFile.getName().endsWith(extension.replace("*", ""))) {
                                            return true;
                                        }
                                    }
                                    return false;
                                }
                                return true;
                            }), project, null);
            chosenFiles.addAll(Arrays.asList(chooser.choose(project, LocalFileSystem.getInstance().findFileByPath($path))));
        };
        ApplicationManager.getApplication().invokeAndWait(openFileChooser);
        List<String> paths = new ArrayList<>();
        for (VirtualFile virtualFile : chosenFiles) {
            paths.add(virtualFile.getPath());
        }
        return paths;
    }

    @Override
    public IPartFactory getPartFactory(final String $projectId) {
        return IdeaPartFactory.getInstance($projectId);
    }

    private Project getProject(final String $projectId) {
        Project[] projects = ProjectManager.getInstance().getOpenProjects();
        for (Project project : projects) {
            if (project.getName().equals($projectId)) {
                return project;
            }
        }
        return null;
    }
}
