/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Idea;

import Com.Wui.Framework.IdeJRE.Idea.Actions.Action;
import Com.Wui.Framework.IdeJRE.Idea.Actions.ActionGroup;
import Com.Wui.Framework.IdeJRE.Idea.Factories.IdeaPartFactory;
import Com.Wui.Framework.IdeJRE.Idea.Runtimes.PluginRuntimeIdea;
import Com.Wui.Framework.IdeJRE.Interfaces.IEnvironmentConfiguration;
import Com.Wui.Framework.IdeJRE.Interfaces.IIdePartConfiguration;
import Com.Wui.Framework.IdeJRE.Loader;
import Com.Wui.Framework.Jcommons.Connectors.FileSystemConnector;
import Com.Wui.Framework.Jcommons.Utils.ObjectDecoder;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.IconLoader;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowAnchor;
import com.intellij.openapi.wm.ToolWindowManager;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.swing.Icon;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Activator implements ProjectComponent {

    private final Project project;
    private static String pluginId;

    public static String getPluginId() {
        return pluginId;
    }

    public Activator(final Project $project) {
        project = $project;
    }

    @Override
    public void projectOpened() {
        try {
            IdeaPartFactory.RegisterProject(project);
            pluginId = parsePluginId();
            PluginRuntimeIdea pluginRuntimeIdea = new PluginRuntimeIdea(pluginId);
            defineParts();
            Loader.Load(pluginRuntimeIdea, project.getName());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void projectClosed() {
        IdeaPartFactory.UnregisterProject(project);
    }

    @Override
    public void initComponent() {
    }

    @Override
    public void disposeComponent() {
    }

    @NotNull
    @Override
    public String getComponentName() {
        return project.getName();
    }


    private String parsePluginId() throws IOException {
        return StringUtils.substringBetween(IOUtils.toString(Activator.class.getClassLoader().getResourceAsStream("META-INF/plugin.xml"),
                "UTF-8"), "<id>", "</id>");
    }

    private void defineParts() throws IOException {
        String appConfigPath = "resource/configs/compiled.config.json";
        if (FileSystemConnector.ExistsResource(appConfigPath)) {
            String appData = FileSystemConnector.ReadResource(appConfigPath);
            IEnvironmentConfiguration dataObj = ObjectDecoder.JSON(appData, IEnvironmentConfiguration.class);

            List<IIdePartConfiguration> views = Arrays.asList(dataObj.views);
            views.forEach((IIdePartConfiguration $idePartConfiguration) -> {
                ToolWindowManager toolWindowManager = ToolWindowManager.getInstance(project);
                String elementId = pluginId + ".Views." + project.getName() + "." + $idePartConfiguration.id;
                String label = $idePartConfiguration.label == null ? $idePartConfiguration.id : $idePartConfiguration.label;
                IdeaPartFactory.RegisterView(elementId, label);
                ToolWindow toolWindow = toolWindowManager.registerToolWindow(label, true, ToolWindowAnchor.RIGHT);
                if ($idePartConfiguration.icon != null) {
                    Icon icon = IconLoader.getIcon("/" + $idePartConfiguration.icon);
                    toolWindow.setIcon(icon);
                }
                IdeaPartFactory.RegisterDefinedPart(elementId);
            });

            List<IIdePartConfiguration> menus = Arrays.asList(dataObj.menus);
            menus.forEach((IIdePartConfiguration $idePartConfiguration) -> {
                DefaultActionGroup actionGroup = (DefaultActionGroup) ActionManager.getInstance().getAction("MainMenu");
                String elementId = Activator.getPluginId() + ".Menus." + $idePartConfiguration.id;
                ActionGroup customGroup = (ActionGroup) ActionManager.getInstance().getAction(elementId);
                if (customGroup == null) {
                    customGroup = new ActionGroup();
                    if ($idePartConfiguration.icon != null) {
                        Icon icon = IconLoader.getIcon("/" + $idePartConfiguration.icon);
                        customGroup.getTemplatePresentation().setIcon(icon);
                    }
                    if ($idePartConfiguration.label != null) {
                        customGroup.getTemplatePresentation().setText($idePartConfiguration.label);
                    }
                    customGroup.setProject(project.getName());
                    customGroup.setPopup(true);
                    ActionManager.getInstance().registerAction(elementId, customGroup);
                    actionGroup.add(customGroup);
                    IdeaPartFactory.RegisterDefinedPart(elementId);
                }
            });

            List<IIdePartConfiguration> tools = Arrays.asList(dataObj.tools);
            tools.forEach((IIdePartConfiguration $idePartConfiguration) -> {
                DefaultActionGroup actionGroup = (DefaultActionGroup) ActionManager.getInstance().getAction("MainToolBar");
                String elementId = Activator.getPluginId() + ".Tools." + $idePartConfiguration.id;
                Action customAction = (Action) ActionManager.getInstance().getAction(elementId);
                if (customAction == null) {
                    customAction = new Action() {
                        @Override
                        public void actionPerformed(final AnActionEvent $anActionEvent) {
                            // todo : invoke eventsmanager with id of element as owner also needs to be linked with project
                            // => $anActionEvent.getProject(), so only one action needs to be inside menu
                        }
                    };
                    if ($idePartConfiguration.icon != null) {
                        Icon icon = IconLoader.getIcon("/" + $idePartConfiguration.icon);
                        customAction.getTemplatePresentation().setIcon(icon);
                    }
                    if ($idePartConfiguration.label != null) {
                        customAction.getTemplatePresentation().setText($idePartConfiguration.label);
                    }
                    customAction.setProject(project.getName());
                    ActionManager.getInstance().registerAction(elementId, customAction);
                    actionGroup.add(customAction);
                    IdeaPartFactory.RegisterDefinedPart(elementId);
                }
            });
        }
    }
}
