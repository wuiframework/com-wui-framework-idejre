/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Idea.Actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;

public abstract class Action extends AnAction {

    private String projectId;

    public Action() {
        super();
    }

    public void setProject(final String $projectId) {
        projectId = $projectId;
    }

    @Override
    public void update(final AnActionEvent $event) {
        /*if ($event.getProject() != null && project != null && project.equals($event.getProject().getName())) {
            $event.getPresentation().setEnabledAndVisible(true);
        } else if ($event.getProject() != null && project != null) {
            $event.getPresentation().setEnabledAndVisible(false);
        }*/
        $event.getPresentation().setText(this.getTemplatePresentation().getText());
        $event.getPresentation().setIcon(this.getTemplatePresentation().getIcon());
    }
}
