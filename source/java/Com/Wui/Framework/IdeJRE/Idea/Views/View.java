/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Idea.Views;

import Com.Wui.Framework.IdeJRE.Idea.Factories.IdeaPartFactory;
import Com.Wui.Framework.IdeJRE.Interfaces.IView;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;

import javax.swing.JComponent;

public class View implements IView {

    private final String id;
    private final String projectId;

    public View(final String $id, final String $projectId) {
        id = $id;
        projectId = $projectId;
    }

    @Override
    public void setContent(final JComponent $content) {
        ToolWindow toolWindow = ToolWindowManager.getInstance(IdeaPartFactory.getProject(projectId)).getToolWindow(id);
        toolWindow.getComponent().add($content);
    }
}
