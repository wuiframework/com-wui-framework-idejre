/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Idea.Menus;

import Com.Wui.Framework.IdeJRE.Idea.Actions.ActionGroup;
import Com.Wui.Framework.IdeJRE.Interfaces.IItem;
import Com.Wui.Framework.IdeJRE.Interfaces.IMenu;

import java.util.Arrays;

public class Menu extends BaseItem implements IMenu {


    public Menu(final ActionGroup $nativeObject, final String $elementId) {
        super($nativeObject, $elementId);
    }

    @Override
    public void addItem(final IItem $item) {
        if ($item instanceof BaseItem) {
            BaseItem baseItem = (BaseItem) $item;
            ActionGroup nativeAction = ((ActionGroup) this.getNativeObject());
            if (!Arrays.asList(nativeAction.getChildActionsOrStubs()).contains(baseItem.getNativeObject())) {
                nativeAction.add(((BaseItem) $item).getNativeObject());
            }
        }
    }
}
