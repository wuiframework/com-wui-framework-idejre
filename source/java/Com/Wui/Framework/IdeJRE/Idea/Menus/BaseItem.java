/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Idea.Menus;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.util.IconLoader;

public class BaseItem {

    private final AnAction nativeObject;
    private String elementId;

    public BaseItem(final AnAction $nativeObject, final String $elementId) {
        elementId = $elementId;
        nativeObject = $nativeObject;
    }

    public AnAction getNativeObject() {
        return nativeObject;
    }

    public String getLabel() {
        return nativeObject.getTemplatePresentation().getText();
    }

    public void setLabel(final String $label) {
        nativeObject.getTemplatePresentation().setText($label);
    }

    public void setIconPath(final String $iconPath) {
        nativeObject.getTemplatePresentation().setIcon(IconLoader.getIcon("/" + $iconPath));
    }

    public String getId() {
        return elementId;
    }

}
