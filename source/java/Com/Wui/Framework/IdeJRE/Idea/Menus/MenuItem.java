/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Idea.Menus;

import Com.Wui.Framework.IdeJRE.Interfaces.IHandledItem;
import com.intellij.openapi.actionSystem.AnAction;

public class MenuItem extends BaseItem implements IHandledItem {

    private final String projectId;

    public MenuItem(final AnAction $nativeObject, final String $elementId, final String $projectId) {
        super($nativeObject, $elementId);
        projectId = $projectId;
    }

    @Override
    public void setHandler(final Runnable $runnable) {
        // todo : add handler to events manager by element id
    }
}
