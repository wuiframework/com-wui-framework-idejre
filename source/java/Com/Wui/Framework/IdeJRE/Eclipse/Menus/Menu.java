/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Eclipse.Menus;

import Com.Wui.Framework.IdeJRE.Interfaces.IMenu;
import Com.Wui.Framework.IdeJRE.Interfaces.IItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenu;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;

import java.util.ArrayList;
import java.util.List;

public class Menu extends BaseItem implements IMenu {

    private List<IItem> items;

    public Menu(final MMenu $nativeObject) {
        super($nativeObject);
        items = new ArrayList<>();
    }

    @Override
    public void addItem(final IItem $item) {
        if (!items.contains($item)) {
            items.add($item);
            if ($item instanceof BaseItem) {
                ((MMenu) this.getNativeObject()).getChildren().add((MMenuElement) ((BaseItem) $item).getNativeObject());
            }
        }
    }
}
