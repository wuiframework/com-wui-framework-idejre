/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Eclipse;

import Com.Wui.Framework.IdeJRE.Eclipse.Runtimes.PluginRuntimeEclipse;
import Com.Wui.Framework.IdeJRE.Loader;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.event.Event;

import javax.inject.Inject;
import javax.inject.Named;

public class Activator {

    private PluginRuntimeEclipse pluginRuntimeEclipse;
    private static MApplication application;
    private static String pluginId;

    public static MApplication getApplication() {
        return application;
    }

    public static String getPluginId() {
        return pluginId;
    }

    @Inject
    @Optional
    public void applicationStarted(@UIEventTopic(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE) Event $event, MApplication $application) {// checkstyle:disable-line
        try {
            application = $application;
            pluginId = FrameworkUtil.getBundle(getClass()).getSymbolicName();
            pluginRuntimeEclipse = new PluginRuntimeEclipse(pluginId);
            Loader.Load(pluginRuntimeEclipse, pluginId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Inject
    @Optional
    public void onSelection(@Named(IServiceConstants.ACTIVE_SELECTION) final ISelection $selection) {
        if ($selection instanceof IStructuredSelection) {
            IStructuredSelection structuredSelection = (IStructuredSelection) $selection;
            Object firstElement = structuredSelection.getFirstElement();
            if (firstElement instanceof IAdaptable) {
                IResource resource = ((IAdaptable) firstElement).getAdapter(IResource.class);
                if (resource != null) {
                    IProject project = resource.getProject();
                    pluginRuntimeEclipse.setProjectPath(project.getName());
                    System.out.println("SETTING CURRENT PROJECT : " + project.getName());
                }
            }
        } else if ($selection instanceof TextSelection) { // e3 implementation follows
            IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
            IEditorPart editor = page.getActiveEditor();
            IEditorInput input = editor.getEditorInput();
            IFile file = input.getAdapter(IFile.class);
            pluginRuntimeEclipse.setProjectPath(file.getProject().getName());
            System.out.println("SETTING CURRENT PROJECT : " + file.getProject().getName());
        }
    }

    @Inject
    @Optional
    public void onPerspectiveChange(@UIEventTopic(UIEvents.UILifeCycle.PERSPECTIVE_OPENED) final Event $event) {
        System.out.println("PERSPECTIVE CHANGED.");
    }
}
