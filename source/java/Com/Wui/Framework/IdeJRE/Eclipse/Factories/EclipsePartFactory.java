/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Eclipse.Factories;

import Com.Wui.Framework.IdeJRE.Eclipse.Activator;
import Com.Wui.Framework.IdeJRE.Eclipse.Menus.Menu;
import Com.Wui.Framework.IdeJRE.Eclipse.Menus.MenuItem;
import Com.Wui.Framework.IdeJRE.Eclipse.Menus.Tool;
import Com.Wui.Framework.IdeJRE.Eclipse.Views.View;
import Com.Wui.Framework.IdeJRE.Interfaces.IMenu;
import Com.Wui.Framework.IdeJRE.Interfaces.IPartFactory;
import Com.Wui.Framework.IdeJRE.Interfaces.IItem;
import Com.Wui.Framework.IdeJRE.Interfaces.ITool;
import Com.Wui.Framework.IdeJRE.Interfaces.IView;
import org.eclipse.e4.ui.model.application.commands.MCommand;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledToolItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenu;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuFactory;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import java.util.List;

public final class EclipsePartFactory implements IPartFactory {

    private int itemNum = 0;

    private static EclipsePartFactory instance;

    public static EclipsePartFactory getInstance() {
        if (instance == null) {
            instance = new EclipsePartFactory();
        }
        return instance;
    }

    @Override
    public IMenu getDefinedMenu(final String $menuId) {
        // menu cannot be searched for directly
        List<MWindow> findElements = Activator.getApplication().getContext()
                .get(EModelService.class).findElements(Activator.getApplication(), null, MWindow.class, null);
        MWindow mainWindow = null;
        for (MWindow window : findElements) {
            if ("IDEWindow".equals(window.getElementId())) {
                mainWindow = window;
            }
        }
        if (mainWindow != null) {
            MMenu mainMenu = mainWindow.getMainMenu();
            List<MMenuElement> elements = mainMenu.getChildren();
            for (MMenuElement element : elements) {
                if ((Activator.getPluginId() + ".Menus." + $menuId).equals(element.getElementId())) {
                    return new Menu((MMenu) element);
                }
            }
        }
        return null;
    }

    @Override
    public IMenu createMenu(final String $projectId) {
        MMenu nativeObj = MMenuFactory.INSTANCE.createMenu();
        nativeObj.setElementId(Activator.getPluginId() + ".Menus." + ++itemNum);
        return new Menu(nativeObj);
    }

    @Override
    public IItem createMenuItem(final String $projectId) {
        MHandledMenuItem nativeObj = MMenuFactory.INSTANCE.createHandledMenuItem();
        nativeObj.setElementId(Activator.getPluginId() + ".MenuItems." + ++itemNum);
        EModelService modelService = Activator.getApplication().getContext().get(EModelService.class);
        List<MCommand> commands = modelService.findElements(Activator.getApplication(),
                Activator.getPluginId() + ".Commands.Singleton", MCommand.class,
                null);
        if (commands.size() == 1) {
            nativeObj.setCommand(commands.get(0));
        }
        return new MenuItem(nativeObj);
    }

    @Override
    public ITool getDefinedTool(final String $toolId) {
        EModelService modelService = Activator.getApplication().getContext().get(EModelService.class);
        List<MHandledToolItem> tools = modelService.findElements(Activator.getApplication(), Activator.getPluginId()
                        + ".Tools." + $toolId,
                MHandledToolItem.class, null);
        if (tools.size() == 1) {
            return new Tool(tools.get(0));
        }
        return null;
    }

    @Override
    public IView getDefinedView(final String $viewId) {
        return new View(Activator.getPluginId()
                + ".Views." + $viewId);
    }

    private EclipsePartFactory() {

    }
}
