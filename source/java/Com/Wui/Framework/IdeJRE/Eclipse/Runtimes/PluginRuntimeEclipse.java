/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

package Com.Wui.Framework.IdeJRE.Eclipse.Runtimes;

import Com.Wui.Framework.IdeJRE.Eclipse.Factories.EclipsePartFactory;
import Com.Wui.Framework.IdeJRE.Interfaces.IPartFactory;
import Com.Wui.Framework.IdeJRE.Interfaces.IPluginRuntime;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class PluginRuntimeEclipse implements IPluginRuntime {

    private final String pluginId;
    private final Map<Integer, String> projectIdPerClientId; // todo : remove on project close?
    private String projectPath = "";

    public PluginRuntimeEclipse(final String $pluginId) {
        pluginId = $pluginId;
        projectIdPerClientId = new ConcurrentHashMap<>();

        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        IResourceChangeListener listener = $event -> System.out.println("RESOURCE CHANGED");
        workspace.addResourceChangeListener(listener);
    }

    @Override
    public void linkClientWithProject(final int $clientId, final String $projectId) {
        if ($projectId == null) {
            return;
        }
        projectIdPerClientId.putIfAbsent($clientId, $projectId);
    }

    @Override
    public String getProjectId(final int $clientId) {
        return projectIdPerClientId.get($clientId);
    }

    @Override
    public String getWorkspaceRoot(final int $clientId) {
        return Platform.getLocation().toString();
    }

    @Override
    public String getPluginId() {
        return pluginId;
    }


    @Override
    public boolean openInfoDialog(final String $title, final String $message, final int $clientId) {
        AtomicBoolean returnValue = new AtomicBoolean(false);
        final Display display = PlatformUI.getWorkbench().getDisplay();
        display.syncExec(() -> {
            MessageBox messageBox = new MessageBox(new Shell(display));
            messageBox.setText($title);
            messageBox.setMessage($message);
            returnValue.set(messageBox.open() > 0);
        });
        return returnValue.get();
    }

    @Override
    public List<String> openFileChooser(final String $path, final String[] $extensions, final int $clientId) {
        List<String> paths = new ArrayList<>();
        final Display display = PlatformUI.getWorkbench().getDisplay();
        display.syncExec(() -> {
            FileDialog dialog = new FileDialog(new Shell(display), SWT.MULTI);
            dialog.setFilterExtensions($extensions);
            dialog.setFilterPath($path);
            dialog.open();
            paths.addAll(Arrays.asList(dialog.getFileNames()));
        });
        return paths;
    }

    @Override
    public IPartFactory getPartFactory(final String $projectId) {
        return EclipsePartFactory.getInstance();
    }

    public void setProjectPath(final String $projectPath) {
        projectPath = $projectPath;
    }
}
