/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.IdeJRE.Eclipse.Views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;

import javax.swing.JComponent;
import java.awt.Frame;
import java.util.HashMap;
import java.util.Map;

public final class ViewContentPool {

    private static Map<String, Composite> compositePerElementId = new HashMap<>();
    private static Map<String, JComponent> contentQueuePerElementId = new HashMap<>();

    private ViewContentPool() {
    }

    public static void registerView(final String $id, final Composite $view) {
        synchronized (ViewContentPool.class) {
            if (contentQueuePerElementId.containsKey($id)) {
                Composite composite = new Composite($view, SWT.EMBEDDED | SWT.NO_BACKGROUND);
                Frame frame = SWT_AWT.new_Frame(composite);
                frame.add(contentQueuePerElementId.get($id));
                contentQueuePerElementId.remove($id);
            }
            compositePerElementId.put($id, $view);
        }
    }

    public static void registerViewContent(final String $id, final JComponent $content) {
        synchronized (ViewContentPool.class) {
            if (compositePerElementId.containsKey($id)) {
                Composite composite = new Composite(compositePerElementId.get($id), SWT.EMBEDDED | SWT.NO_BACKGROUND);
                Frame frame = SWT_AWT.new_Frame(composite);
                frame.add($content);
            } else {
                contentQueuePerElementId.put($id, $content);
            }
        }
    }

}
